Tutanota Plain Text Bookmarklet
===============================
A simple bookmarklet that helps when viewing plain text emails in Tutanota. The Tutanota team
is [working on](https://github.com/tutao/tutanota/issues/1588) a plain text mode, but while we
wait this can help.

Just copy the code below to a new bookmark in your toolbar. Then, click it when you are on the
Tutanota app with an email open. It should change the email body to use a fixed width font
which helps with the formatting of plain text emails.

```javascript
javascript:(function(){if(document.getElementById("mail-body")&&(document.body.classList.toggle("tutanota-plain-text-fix"),!document.getElementById("tutanota-plain-text-fix-styles"))){var a=document.createElement("style"),b=document.createTextNode(".tutanota-plain-text-fix #mail-body { font-family: monospace !important; white-space: pre !important; }");a.appendChild(b);a.id="tutanota-plain-text-fix-styles";document.head.appendChild(a)}})();
```
