(function () {

    // Define some strings to use later
    var className = 'tutanota-plain-text-fix';
    var tagId = 'tutanota-plain-text-fix-styles';
    var contentElId = 'mail-body';

    // Check if we are on the right page
    var el = document.getElementById(contentElId);

    if (! el) { return; }

    // Toggle our styling class
    document.body.classList.toggle(className);

    // Add the style block if it's not already there
    var styleTag = document.getElementById(tagId);

    if (styleTag) { return; }

    var style = document.createElement('style');

    var css = document.createTextNode('.' + className + ' #' + contentElId + ' { font-family: monospace !important; white-space: pre !important; }');

    style.appendChild(css);
    style.id = tagId;

    document.head.appendChild(style);
}());
